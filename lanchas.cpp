#include <iostream>
#include "lanchas.h"


//Constructor
lanchas::lanchas(std::string pNombre, int pDistancia, int pNitro, int pVelocidad) {
	
	nombre = pNombre;
	distancia = pDistancia;
	nitro = pNitro;
	velocidad = pVelocidad;
}

//Getters 
std::string lanchas::getNombre() {
	return nombre;
}

int lanchas::getDistancia()
{
	return distancia;
}

int lanchas::getNitro()
{
	return nitro;
}

int lanchas::getVelocidad()
{
	return velocidad;
}


//Setters

void lanchas::setNombre(std::string pNombre)
{
	nombre = pNombre;
}

void lanchas::setDistancia(int pDistancia)
{
	distancia = pDistancia;
}

void lanchas::setNitro(int pNitro)
{
	nitro = pNitro;
}

void lanchas::setVelocidad(int pVelocidad)
{
	velocidad = pVelocidad;
}

//CONSTRUCION 

void lanchas::printarlanchas() {

	std::cout << " La lancha que la controla " << nombre << " tiene una distancia recorrida de " << distancia << " actualmente una velocidad " << velocidad << " y tiene el nitro a " <<nitro<< "\n";

}
