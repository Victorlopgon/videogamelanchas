#pragma once
#include <string>

class lanchas
{
private:
	int velocidad;
	int distancia;
	int nitro;
	std::string nombre;
public:

	//constructor
	lanchas(std::string pnombre, int pDistancia , int pNitro , int pVelocidad);


	//constructor
	//getters
	std::string getNombre();
	int getDistancia();
	int getNitro();
	int getVelocidad();

	//setters
	void setNombre(std::string pNombre);
	void setDistancia(int pDistancia);
	void setNitro(int pNitro);
	void setVelocidad(int pVelocidad);

	//construcion
	void printarlanchas();

	


};
